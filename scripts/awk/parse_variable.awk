BEGIN{
	FS=" \\* "
	OFS="\t"
	print "parameter","pi","method","comment"
}

/^Parameter/{
	a=1
	sub(/^Parameter\(s\):/,"")
}

a==1&&/^\t/{
	sub(/^\t/,"")
	for (i=2;i<=NF;i++){
		if($i ~ /^[A-Z/]+:/){
			n=index($i,":")
			split($i,tmp,": ")
			var[tmp[1]]=substr($i,n+1)
		}
	}
	print $1,var["PI"],var["METHOD/DEVICE"],var["COMMENT"]
	split("",var)
	next
}

a==1{
	exit
}


